//import 'dart:html';
import 'package:cloud_firestore/cloud_firestore.dart';
//import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

class OrderList extends StatefulWidget {
  const OrderList({Key? key}) : super(key: key);

  @override
  State<OrderList> createState() => _OrderListState();
}

class _OrderListState extends State<OrderList> {
  final Stream<QuerySnapshot> _myOrders =
      FirebaseFirestore.instance.collection("orders").snapshots();

  @override
  Widget build(BuildContext context) {
    TextEditingController _cakedesignCtrlr = TextEditingController();
    TextEditingController _categoryCtrlr = TextEditingController();

    void _delete(docId) {
      FirebaseFirestore.instance
          .collection("orders")
          .doc(docId)
          .delete()
          .then((value) => print("Data deleted successfully"));
    }

    void _update(data) {
      var collection = FirebaseFirestore.instance.collection("orders");
      _cakedesignCtrlr.text = data["Cake_Design"];
      _categoryCtrlr.text = data["Category"];

      showDialog(
        context: context,
        builder: (_) => AlertDialog(
          title: const Text("Update"),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              TextField(
                controller: _cakedesignCtrlr,
              ),
              TextField(
                controller: _categoryCtrlr,
              ),
              TextButton(
                  onPressed: () {
                    collection.doc(data["doc_id"]).update({
                      "Cake_Design": _cakedesignCtrlr.text,
                      "Category": _categoryCtrlr.text,
                    });
                    Navigator.pop(context);
                  },
                  child: Text("Update"))
            ],
          ),
        ),
      );
    }

    return StreamBuilder(
      stream: _myOrders,
      builder: (BuildContext context,
          AsyncSnapshot<QuerySnapshot<Object?>> snapshot) {
        if (snapshot.hasError) {
          return Text("Something went wrong");
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return CircularProgressIndicator();
        }

        if (snapshot.hasData) {
          return Row(
            children: [
              Expanded(
                  child: SizedBox(
                height: (MediaQuery.of(context).size.height),
                width: (MediaQuery.of(context).size.width),
                child: ListView(
                  children: snapshot.data!.docs
                      .map((DocumentSnapshot documentSnapshot) {
                    Map<String, dynamic> data =
                        documentSnapshot.data()! as Map<String, dynamic>;

                    return Column(
                      children: [
                        Card(
                            child: Column(
                          children: [
                            ListTile(
                              title: Text(data['Cake_Design']),
                              subtitle: Text(data['Category']),
                            ),
                            ButtonTheme(
                              child: ButtonBar(
                                children: [
                                  OutlinedButton.icon(
                                    onPressed: () {
                                      _update(data);
                                    },
                                    icon: Icon(Icons.edit),
                                    label: Text("Edit"),
                                  ),
                                  OutlinedButton.icon(
                                    onPressed: () {
                                      _delete(data["doc_id"]);
                                    },
                                    icon: Icon(Icons.remove),
                                    label: Text("Delete"),
                                  )
                                ],
                              ),
                            )
                          ],
                        ))
                      ],
                    );
                  }).toList(),
                ),
              ))
            ],
          );
        } else {
          return (Text("Data does not exit"));
        }
      },
    );
  }
}

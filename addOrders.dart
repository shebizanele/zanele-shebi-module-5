import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:module_4/orderList.dart';
import 'package:firebase_core/firebase_core.dart';

class AddOrder extends StatefulWidget {
  const AddOrder({Key? key}) : super(key: key);

  @override
  State<AddOrder> createState() => _AddOrderState();
}

class _AddOrderState extends State<AddOrder> {
  @override
  Widget build(BuildContext context) {
    TextEditingController cakedesignController = TextEditingController();
    TextEditingController categorynController = TextEditingController();
    TextEditingController contact_noController = TextEditingController();

    Future _addOrders() {
      final cakedesign = cakedesignController.text;
      final category = categorynController.text;
      final contact_no = contact_noController.text;

      //final ref = FirebaseFireStore.instance.collection(orders).doc();
      final ref = FirebaseFirestore.instance.collection("orders").doc();

      return ref
          .set({
            "Cake_Design": cakedesign,
            "Category": category,
            "Contact_no": contact_no,
            "doc_id": ref.id
          })
          .then((value) => log("Order added!!!"))
          .catchError((onError) => log(onError));
    }

    return Column(
      children: [
        Column(
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
              child: TextField(
                controller: cakedesignController,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20)),
                    hintText: "Please enter your cake design"),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
              child: TextField(
                controller: categorynController,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20)),
                    hintText:
                        "Please enter your cake category, eg. Wedding, Birthday etc"),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
              child: TextField(
                controller: contact_noController,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20)),
                    hintText: "Please enter your contact details"),
              ),
            ),
            ElevatedButton(
              onPressed: () {
                _addOrders();
              },
              child: Text("Add Order"),
            )
          ],
        ),
        OrderList(),
      ],
    );
  }
}

import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'addOrders.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
      options: FirebaseOptions(
          apiKey: "AIzaSyDGK_asWndFFoLWTf3KzaCsNlZ1kRNpEdc",
          authDomain: "module04-a757c.firebaseapp.com",
          projectId: "module04-a757c",
          storageBucket: "module04-a757c.appspot.com",
          messagingSenderId: "998713334037",
          appId: "1:998713334037:web:4927f206ef995588cab11c"));

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Ps Cakes',
      theme: ThemeData(
        primarySwatch: Colors.indigo,
      ),
      home: const MyHomePage(title: 'Cake Orders'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: Center(
          // Center is a layout widget. It takes a single child and positions it
          // in the middle of the parent.
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[AddOrder()],
          ),
        ) // This trailing comma makes auto-formatting nicer for build methods.
        );
  }
}
